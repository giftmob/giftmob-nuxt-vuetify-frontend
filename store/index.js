export const state = () => ({
    token: '',
    loggedIn: false
})

export const mutations = {
    token (state, token) {
        state.token = token
    },
    loggedIn (state, loggedIn) {
        state.loggedIn = loggedIn
    }
}

export const getters = {
    token: state => state.token,
    loggedIn: state => state.loggedIn
}
