import axios from 'axios'

export default {
  getApi: (token) => {
    const baseURL = process.env.BACKEND_URL

    const headers = {
      'Content-Type': 'application/json'
    }
    if (token) {
      headers.Authorization = 'Bearer ' + token
    }

    const api = axios.create({
      baseURL,
      headers
    })

    return api
  }
}
